module.exports = function(grunt) {
  grunt.initConfig({
    pkg:grunt.file.readJSON('package.json'),
    /* 
       SASS: This compiles SASS stylesheets into a css stylesheet. 
       - https://github.com/gruntjs/grunt-contrib-sass
    */
    sass:{
      compile:{
          files:{
            'stylesheets/app.css' : 'sass/app.scss'
          }
      }
    },   
    /* 
      WATCH: Watching .html files and .less file for changes.
      If changes are made, then run functions and livereload.
      - https://github.com/gruntjs/grunt-contrib-watch
    */
    watch: {
      css: {
        files: ['sass/*.scss','*.html','*.htm'],
        // tasks: ['sass'],
        options: {
          livereload: true
        },
      },
    },
    /* 
      CONNECT: Start localhost server.
      - https://github.com/gruntjs/grunt-contrib-connect
    */
    connect: {
      all: {
        options:{
          port: 9000,
          hostname: '0.0.0.0',
          livereload: true
        }
      }
    },
    /*
      OPEN: will open your browser at the project's URL
      - https://github.com/jsoverson/grunt-open
    */
    open: {
      all: {
        // Gets the port from the connect configuration
        path: 'http://localhost:<%= connect.all.options.port%>/',
        // Application it will be opened in
        app: 'Chrome'
      }
    }
  });

  // Load NPM Tasks
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-open');
  

  // Default actions 
  grunt.registerTask('default',['sass']);


 // Development Actions
 // - This is what you will want to run while developing for autoreload
  grunt.registerTask('dev', ['connect', 'open', 'watch']);

};