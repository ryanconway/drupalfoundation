<div style="background: #333; color:#fff; padding: 1em 0px">
  <div class="row">
    <div class="large-6 columns">
      <h5 style="color:#fff; margin:0; padding:0;">Medical Information Technolgy, Inc.</h5>
    </div>
    <div class="large-6 columns">
      <ul style="list-style-type:none;" >
        <li style="float:left; margin-right:1.5em;">Privacy Policy</li>
        <li style="float:left; margin-right:1.5em;">Copyright Info</li>
        <li style="float:left;">Terms &amp; Conditions</li>
      </ul>
    </div>
  </div>
</div>


<script src="javascripts/vendor/jquery.js"></script>
  <script src="javascripts/foundation/foundation.js"></script>

  <!-- Other JS plugins can be included here -->

  <script>
   var searchvisible = 0;
    $("#search-menu").click(function(e){
        //This stops the page scrolling to the top on a # link.
        e.preventDefault();
        if (searchvisible ===0) {
            //Search is currently hidden. Slide down and show it.
            $("#search-form").slideDown(200);
            $("#s").focus(); //Set focus on the search input field.
            searchvisible = 1; //Set search visible flag to visible.
        } else {
            //Search is currently showing. Slide it back up and hide it.
            $("#search-form").slideUp(200);
            searchvisible = 0;
        }
    });





    $(document).foundation();
  </script>

   <!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <!-- Slidebars -->
    <script src="javascripts/menu/slidebars.min.js"></script>
    <script>
      (function($) {
        $(document).ready(function() {
          $.slidebars();
        });
      }) (jQuery);
    </script>
    <!-- End Hidden Mobile Nav -->



</div><!-- #page -->
</body>
</html>

