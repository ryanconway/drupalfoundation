
<!-- HEADER -->
<?php include("_includes/header.inc"); ?>

<!-- HERO IMG -->
<?php include("_includes/hero.inc"); ?> 

<!-- EHR SOLUTIONS -->
<?php include("_includes/ehrsolutions.inc"); ?>

<!-- UPCOMING EVENTS -->
<?php include("_includes/upcomingevents.inc"); ?>

<!-- NEWS SECTION -->
<?php include("_includes/news.inc"); ?>

<!-- OUR STORY SECTION -->
<?php include("_includes/ourstory.inc"); ?>

<!-- CONTACT US Form -->
<?php // include("_includes/contactusform.inc"); ?>

<!-- SOCIAL MEDIA BUTTONS -->
<?php // include('_includes/socialmedia.inc'); ?>

<!-- CONTACT US MAP -->
<?php include('_includes/contactusmap.inc'); ?>

<!-- SITEMAP -->
<?php include('_includes/sitemap.inc'); ?>

<!-- FOOTER -->
<?php include('_includes/footer.inc'); ?>