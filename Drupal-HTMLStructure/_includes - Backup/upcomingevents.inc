<div class="row">
    <div class="large-12 columns">
      <h2>Upcoming Events</h2>
    </div>
  </div>

  <!--  TOP ROW -->

  <div class="row">

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

  </div>

  <br><br>

  <!--  SECOND ROW -->

  <div class="row">

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

  </div>

  <br><br>

  <!--  THIRD ROW -->

  <div class="row">

    <div class="large-4 medium-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 medium-4 columns">

          <div class="medium-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="medium-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

    <div class="large-4 medium-4 columns">

          <div class="large-4 small-12 columns">
            <div class="datebox">
              <p>April <br>23</p>
            </div>
          </div>

          <div class="large-8 small-12 columns">
            <p>Regional Event<br>
              Crystal City, VA</p>
          </div>

    </div>

  </div>

  <br><br>

  <!-- BUTTON -->

    <div class="row">
      <div class="large-12 columns" style="text-align:right">
        <p><a href="#" class="large button">Customer Events</a></p>
      </div>
      <hr>
    </div>