<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>MEDITECH</title>

  <!-- FOUNDATION CSS -->
  <link rel="stylesheet" href="stylesheets/app.css">

  <!-- Slidebars CSS -->
  <link rel="stylesheet" href="stylesheets/menu/slidebars.min.css">
  <link rel="stylesheet" href="stylesheets/menu/example-styles.css">
  
  <!-- TEMPORARY INLINE STYLES -->
  <style type="text/css">
    #map {
      width: 100%;
      height: 400px;
      z-index: -1000;
    }
    #search-form {
      display: none;
      padding: 10px;
      width: 100%;
    }
    .searchform{
      width:100%;
    }
    input[type="text"]{
      display:inline-block;
      width:52%;
      height:3.4em;
    }
    .searchbtn{
      height:3em;
    }
    .sb-menu li{
      list-style: none;
    }
      .sb-menu li:first-child{
      border-top:none;
    }
    .search{
      width:100%;
      text-align:right;
    }
    .datebox{
      width:5em;
      height:5em;
      padding: 1em;
      text-align: center;
      background: #f2f2f2;
      border:1px solid #d9d9d9;
    }
  </style>

  <!-- Modinizer -->
  <script src="javascripts/vendor/custom.modernizr.js"></script>

  <!-- Google Maps API -->
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>

  <!-- Google Maps API - Snazzy Maps - Script -->
  <script type="text/javascript">
      google.maps.event.addDomListener(window, 'load', init);
      function init() {
          var mapOptions = {
              zoom: 15,
              center: new google.maps.LatLng(42.222403, -71.174153),
              styles: [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}],
              scrollwheel: false,
              navigationControl: false,
              mapTypeControl: false,
              scaleControl: false,
          };
          var mapElement = document.getElementById('map');
          var map = new google.maps.Map(mapElement, mapOptions);
      }
  </script>
</head>
<body>

  <!-- STICKY NAV -->
   <div id="fixed contain-to-grid sb-site">
    <nav id="mobilenav" class="show-for-small">
      <ul class="left">
        <li class="first"><a class="menu-icon sb-toggle-left"><img style="width: 30px; height: 30px" src="http://help.alamode.com/appraiser/totalforAndroid2013/Images/AndroidMenuIcon.png" alt=""></a></li>
        <li><b>MEDITECH</b></li>
      </ul>
      <ul class="right">
        <li style="padding-left:0;">
            <a id="search-menu" href="#"><img src="https://cdn3.iconfinder.com/data/icons/wpzoom-developer-icon-set/500/67-128.png" style="width: 30px; height:30px; margin-left: 10px; padding-right: 2px; margin-right:0px;"></a>
        </li>
        <li style="padding-left:0;"><a class="menu-icon sb-toggle-right" href=""><img src="https://cdn3.iconfinder.com/data/icons/users/100/user_male_1-512.png" style="width: 30px; height:30px; margin-left: 10px;"></a></li>
      </ul>
      <div class="large-12 columns show-for-medium-down">
        <div id="search-form">
            <form style="margin-bottom:0px;"class="searchform" role="search" method="get" id="searchform" action="">
                <input class="searchfield" type="text" value="" name="s" id="s" />
                <input class="button" type="submit" id="searchsubmit" value="Search" />
            </form>
        </div>
      </div>
      <div class="clear"></div>
    </nav>
    <div id="header" class="hide-for-small">
      <div id="top-header-bar">
        <div class="row">
          <div class="columns">
            <a class="loginButton right" href="#">Customer Login</a>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="large-6 columns" style="margin-top:20px;">
          <h1 class="left">MEDITECH</h1>
        </div>
        <div class="large-6 columns" style="margin-top:20px;">
          <div class="search">
            <input type="text" placeholder="Search">
            <input class="button" type="submit" id="searchsubmit" value="Login" />
          </div>
        </div><!-- End of search -->
      </div>
      <div class="row">
        <div class="large-12 columns">
          <ul class="menu">
            <li class="first"><a href="#">EHR Solutions</a></li>
            <li><a href="#">Regulatory</a></li>
            <li><a href="#">Events</a></li>
            <li><a href="#">News</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
          </ul>
        </nav>
        </div>
      </div><!-- .row -->
    </div><!-- #header -->
    <div class="sb-slidebar sb-left">
      <!-- Your left Slidebar content. -->
      <nav>
        <ul class="sb-menu">
            <li><a href="#">EHR Solutions</a></li>
            <li><a href="#">Regulatory</a></li>
            <li><a href="#">Events</a></li>
            <li><a href="#">News</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
      </nav>
    </div>
    <div class="sb-slidebar sb-right">
      <!-- Your left Slidebar content. -->
      <h2 style="color: #FFF; line-height: 160%;">Please login to your MEDITECH account</h2>
      <nav>
        <ul class="sb-menu">
            <li style="border:none;"><input style="width: 100%;" type="text" placeholder="Username"></li>
            <li style="border:none;"><input style="width: 100%;" type="text" placeholder="Password"></li>
            <li style="border:none;"><input style="padding: 10px 0px" class="button expand" type="submit" id="searchsubmit" value="Login" /></li>
        </ul>
      </nav>
      <p>Trouble logging in?<br>
      Please visit <a href="#" style="color:#009abc;">our login page</a> for more detailed instructions.</p>
    </div>
  </div><!-- .sb-site -->