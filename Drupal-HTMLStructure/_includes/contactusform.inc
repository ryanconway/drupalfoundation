<div class="row">
    <div class="large-12 columns">
      <h2>Contact US</h2>
    </div>
    <div class="row">
      <div class="large-6 columns">
        <label>Name
          <input type="text" placeholder="Name" />
        </label>
        <label>E-mail
          <input type="text" placeholder="E-mail" />
        </label>
        <label>Phone
          <input type="text" placeholder="Phone" />
        </label>
        <label>Organization
          <input type="text" placeholder="Organization" />
        </label>
        <label>State/Province
          <input type="text" placeholder="State/Province" />
        </label>
      </div>
      <div class="large-6 columns">
        <label>Message
          <textarea placeholder="Message"></textarea>
        </label>
        <p><a href="#" class=" large-12 large button">Submit</a></p>
      </div>
    </div>
  <hr>
</div>