  <!-- EHR Solutions Section -->
  <div class="row">
    <div class="row">
      <div class="large-12 columns">
        <h2>EHR Solutions</h2>
        <p>See for yourself why our customer community of hospitals, ambulatory care centers, physicians' offices, home care organizations, long term care and behavioral health facilities choose MEDITECH.</p>
      </div>
    </div><!-- .row -->
    <div class="row">
      <div class="large-12 columns">
        <ul class="stats small-block-grid-1 medium-block-grid-2 large-block-grid-3">
          <li>
            <h3>1300+</h3>
            <h4>customers LIVE with <br> a full HCIS </h4>
          </li>
          <li>
            <h3>2300+</h3>
            <h4>customers in over <br> 10 countries </h4>
          </li>
          <li>
            <h3>580+</h3>
            <h4>customers successfully <br> attested for Meaningful Use </h4
          </li>
        </ul>
      </div>
    </div><!-- .row -->
    <div class="row">
      <div class="large-12 columns">
        <h2 style="text-align:center">THEIR SOLUTION. WE CAN BE YOURS TOO.</h2>
      </div>
    </div><!-- .row -->
    <div class="row">
      <div class="large-4 columns">
      </div>
      <div class="large-4 columns">
      </div>
      <div class="large-4 columns">
        <p><a href="#" class="large button">Our EHR Solutions</a></p>
      </div>
    </div><!-- .row -->
  </div><!-- .row -->
  <!-- End of EHR Solutions Section -->