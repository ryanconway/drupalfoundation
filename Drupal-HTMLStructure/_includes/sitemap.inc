<div class="row">
  <div class="large-4 columns">
    <h4><a href="about.htm">About</a></h4>
    <ul>
      <li><a href="our-executives.htm">Our Executives</a></li>
      <li><a href="#">Community</a></li>
      <li><a href="#">Directions</a></li>
    </ul>

    <h4><a href="ehr-solutions.htm">EHR Solutions</a></h4>
    <ul>
      <li><a href="#">Physician</a></li>
      <li><a href="#">Nursing</a></li>
      <li><a href="#">Clinical</a></li>
      <li><a href="#">HIM</a></li>
      <li><a href="#">Executive</a></li>
      <li><a href="#">Financial</a></li>
      <li><a href="#">Home Health</a></li>
      <li><a href="#">IT Staff</a></li>
    </ul>
  </div>

  <div class="large-4 columns">
    <h4><a href="regulatory.htm">Regulatory</a></h4>
    <ul>
      <li><a href="#">ARRA Resources</a></li>
      <li><a href="#">Best Practices</a></li>
      <li><a href="#">Grouper Files</a></li>
      <li><a href="#">APC Files</a></li>
      <li><a href="#">Canadian Files</a></li>
      <li><a href="#">ICD-10</a></li>
      <li><a href="#">HIPAA &amp; Regulatory</a></li>
      <li><a href="#">Joint Commission</a></li>
      <li><a href="#">Canadian Regulatory</a></li>
    </ul>
    <h4><a href="news.htm">News</a></h4>
    <ul>
      <li><a href="#">Press Appearances</a></li>
      <li><a href="#">Articles</a></li>
      <li><a href="#">Stage 6 &amp; 7</a></li>
    </ul>
  </div>

  <div class="large-4 columns">
    <h4><a href="events.htm">Events</a></h4>
      <ul>
        <li><a href="#">HIMSS 2015</a></li>
        <li><a href="#">CIO Forum</a></li>
        <li><a href="#">Nurse Forum</a></li>
      </ul>
    <h4><a href="careers.htm">Careers</a></h4>
    <h4><a href="contact.htm">Contact</a></h4>
    <h4><a href="contact.htm">Shareholders</a></h4>
    <h4>Follow Us</h4>
      <ul>
        <li><a href="#">Twitter</a></li>
        <li><a href="#">Facebook</a></li>
        <li><a href="#">Youtube</a></li>
      </ul>
  </div>

</div>