<div class="row">
    <div class="large-12 columns">
      <h2>News</h2>
    </div>

  <div class="row">

    <div class="large-6 columns">
      <h5>Phoebe Putney Health System Picks MEDITECH 6.1&nbsp;EHR</h5>
      <p>Phoebe Putney Memorial Hospital, a 691-bed teaching hospital in Albany, GA, has chosen to implement MEDITECH’s 6.1 electronic health record (EHR) after careful consideration of several EHR&nbsp;vendors.</p>
      <p>01 May</p>
    </div>

    <div class="large-6 columns">
      <h5>Teaching Hospital Berkshire Medical Center Learns the Benefits of MEDITECH’s PDoc</h5>
      <p>Berkshire Medical Center (Pittsfield, MA) is a quick study when it comes to learning the value of MEDITECH’s Physician Documentation (PDoc).</p>
      <p>01 May</p>
    </div>

  </div>

  <div class="row">

    <div class="large-6 columns">
      <h5>With MEDITECH’s EHR, Doctors Hospital Delivers Integrated Care to Island Patients</h5>
      <p>For many people, life in the Bahamas suggests crystalline blue waters, chaise lounges, and white sandy beaches.</p>
      <p>01 May</p>
    </div>

    <div class="large-6 columns">
      <h5>Phoebe Putney Health System Picks MEDITECH 6.1&nbsp;EHR</h5>
      <p>Phoebe Putney Memorial Hospital, a 691-bed teaching hospital in Albany, GA, has chosen to implement MEDITECH’s 6.1 electronic health record (EHR) after careful consideration of several EHR vendors.</p>
      <span style="font-style:italic"> 01 May</span>
    </div>

  </div>

  <br>

  <div class="row">

    <div class="large-6 columns">
      <h5>Teaching Hospital Berkshire Medical Center Learns the Benefits of MEDITECH’s PDoc</h5>
      <p>Berkshire Medical Center (Pittsfield, MA) is a quick study when it comes to learning the value of MEDITECH’s Physician Documentation (PDoc).</p>
      <span style="font-style:italic"> 01 May</span>
    </div>

    <div class="large-6 columns">
      <h5>With MEDITECH’s EHR, Doctors Hospital Delivers Integrated Care to Island Patients</h5>
      <p>For many people, life in the Bahamas suggests crystalline blue waters, chaise lounges, and white sandy beaches.</p>
      <span style="font-style:italic"> 01 May</span>
    </div>

  </div>

  <div class="row">
      <div class="large-4 columns">

      </div>
      <div class="large-4 columns">

      </div>
      <div class="large-4 columns">
        <p><a href="#" class="large button">More News</a></p>
      </div>
    </div>

    <hr>
    </div>