
<!-- HEADER -->
<!-- Ryan -->
<?php include("_includes/header.inc"); ?>

<!-- HERO IMG -->
<!-- Ryan -->
<?php include("_includes/hero.inc"); ?> 

<!-- EHR SOLUTIONS -->
<!-- Done -->
<?php include("_includes/ehrsolutions.inc"); ?>

<!-- UPCOMING EVENTS -->
<!-- Max -->
<?php include("_includes/upcomingevents.inc"); ?>

<!-- NEWS SECTION -->
<!-- Max -->
<?php include("_includes/news.inc"); ?>

<!-- OUR STORY SECTION -->
<!-- Joe -->
<?php include("_includes/ourstory.inc"); ?>

<!-- CONTACT US Form -->
<?php // include("_includes/contactusform.inc"); ?>

<!-- SOCIAL MEDIA BUTTONS -->
<?php // include('_includes/socialmedia.inc'); ?>

<!-- CONTACT US MAP -->
<!-- Joe -->
<?php include('_includes/contactusmap.inc'); ?>

<!-- SITEMAP -->
<!-- Charles -->
<?php include('_includes/sitemap.inc'); ?>

<!-- FOOTER -->
<!-- Charles -->
<?php include('_includes/footer.inc'); ?>