<?php
/*

*	File:		index.php
*	By:		Ryan
*	Date:		3/18/13

=====================================
*/

echo "This is the first index page";
echo "<br />";
echo "<br />";
echo "by: Ryan Conway";

//      3 x 3 table with data
echo "<table>";
	echo "<tr>";
		echo "<td>ID</td>";
		echo "<td>Company</td>";
		echo "<td>Postcode</td>";
	echo "</tr>";
	echo "<tr>";
		echo "<td>201</td>";
		echo "<td>TMIT</td>";
		echo "<td>OX26</td>";
	echo "</tr>";
	echo "<tr>";
		echo "<td>203</td>";
		echo "<td>Another Company</td>";
		echo "<td>SW3</td>";
	echo "</tr>";
echo "</table>";
?>
