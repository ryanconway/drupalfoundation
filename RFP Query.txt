SELECT  TOP (100) PERCENT RFPID, MSRRespDueDate, LastEditDate, LastEditBy, Location, FacilityNotes, CurrentVendor, CurrentlyOwn, Category, RFPStatus, DateCompleted, TimeCompleted, 
               ClarificationQuestionsDate, ClarificationQuestionsTime, ClarificationQuestionsTo, MSRRespDueTime, RFPNotes, SalesRespBy, SalesResponseByTime, SalesRespDueDate, SalesEditsDueTime, 
               RFPMailDate, RFPMailTime, RFPDueToSite, RFPDueToSiteTime, PromoWriter, PromoWriterName, SolutionNotes, GenDirections, NarrResponse, AnswerKeyResponse, YesNoResponse, MktNotes, 
               SampleResponse, PromoWriterColor, SalesResp, SalesColor, HardwareINTResp, HardwareINTColor, HardwareEXTResp, HardwareEXTColor, InterfacesResp, InterfacesColor, TechnicalResp, 
               TechnicalColor, LegalResp, LegalColor, StatisticsResp, StatisticsColor, Group1, Group2, Group3, Group4, Group5, Group6, Group7, Group8, Group9, Group10, Group11, Group12, Group13, Group14, 
               Group15, Group16, Group17, Group18, Group19, Group20, Group21, Group22, Group23, Group24, Group25, Group26, Group27, Group28, Group29, Group30, Group31, Group32, Group33, Group34, 
               Group35, Group36, Group37, Group38, Group39, Group40, Group41, Group42, Group43, Group44, Group1Name, Group2Name, Group3Name, Group4Name, Group5Name, Group6Name, Group7Name, 
               Group8Name, Group9Name, Group10Name, Group11Name, Group12Name, Group13Name, Group14Name, Group15Name, Group16Name, Group17Name, Group18Name, Group19Name, 
               Group20Name, Group21Name, Group22Name, Group23Name, Group24Name, Group25Name, Group26Name, Group27Name, Group28Name, Group29Name, Group30Name, Group31Name, 
               Group32Name, Group33Name, Group34Name, Group35Name, Group36Name, Group37Name, Group38Name, Group39Name, Group40Name, Group41Name, Group42Name, Group43Name, 
               Group44Name, Group1Color, Group2Color, Group3Color, Group4Color, Group5Color, Group6Color, Group7Color, Group8Color, Group9Color, Group10Color, Group11Color, Group12Color, Group13Color, 
               Group14Color, Group15Color, Group16Color, Group17Color, Group18Color, Group19Color, Group20Color, Group21Color, Group22Color, Group23Color, Group24Color, Group25Color, Group26Color, 
               Group27Color, Group28Color, Group29Color, Group30Color, Group31Color, Group32Color, Group33Color, Group34Color, Group35Color, Group36Color, Group37Color, Group38Color, Group39Color, 
               Group40Color, Group41Color, Group42Color, Group43Color, Group44Color, ResponseFormat, Copies, ShipInstructions, ShipContactInfo, AddDocuments, ProspWebSiteUserName, 
               ProspWebSitePassword
FROM     dbo.tblRFPAssignment
WHERE  (PromoWriter IN ('LACH', 'LATOUR') OR
               Group1Name IN ('LACH', 'LATOUR') OR
               Group2Name IN ('LACH', 'LATOUR') OR
               Group3Name IN ('LACH', 'LATOUR') OR
               Group4Name IN ('LACH', 'LATOUR') OR
               Group5Name IN ('LACH', 'LATOUR') OR
               Group6Name IN ('LACH', 'LATOUR') OR
               Group7Name IN ('LACH', 'LATOUR') OR
               Group8Name IN ('LACH', 'LATOUR') OR
               Group9Name IN ('LACH', 'LATOUR') OR
               Group10Name IN ('LACH', 'LATOUR') OR
               Group11Name IN ('LACH', 'LATOUR') OR
               Group12Name IN ('LACH', 'LATOUR') OR
               Group13Name IN ('LACH', 'LATOUR') OR
               Group14Name IN ('LACH', 'LATOUR') OR
               Group15Name IN ('LACH', 'LATOUR') OR
               Group16Name IN ('LACH', 'LATOUR') OR
               Group17Name IN ('LACH', 'LATOUR') OR
               Group18Name IN ('LACH', 'LATOUR') OR
               Group19Name IN ('LACH', 'LATOUR') OR
               Group20Name IN ('LACH', 'LATOUR') OR
               Group21Name IN ('LACH', 'LATOUR') OR
               Group22Name IN ('LACH', 'LATOUR') OR
               Group23Name IN ('LACH', 'LATOUR') OR
               Group24Name IN ('LACH', 'LATOUR') OR
               Group25Name IN ('LACH', 'LATOUR') OR
               Group26Name IN ('LACH', 'LATOUR') OR
               Group27Name IN ('LACH', 'LATOUR') OR
               Group28Name IN ('LACH', 'LATOUR') OR
               Group29Name IN ('LACH', 'LATOUR') OR
               Group30Name IN ('LACH', 'LATOUR') OR
               Group31Name IN ('LACH', 'LATOUR') OR
               Group32Name IN ('LACH', 'LATOUR') OR
               Group33Name IN ('LACH', 'LATOUR') OR
               Group34Name IN ('LACH', 'LATOUR') OR
               Group35Name IN ('LACH', 'LATOUR') OR
               Group36Name IN ('LACH', 'LATOUR') OR
               Group37Name IN ('LACH', 'LATOUR') OR
               Group38Name IN ('LACH', 'LATOUR') OR
               Group39Name IN ('LACH', 'LATOUR') OR
               Group40Name IN ('LACH', 'LATOUR') OR
               Group41Name IN ('LACH', 'LATOUR') OR
               Group42Name IN ('LACH', 'LATOUR') OR
               Group43Name IN ('LACH', 'LATOUR') OR
               Group44Name IN ('LACH', 'LATOUR')) AND (MSRRespDueDate BETWEEN '2012-03-01 00:00:00.000' AND '2013-01-15 00:00:00.000')
ORDER BY MSRRespDueDate DESC